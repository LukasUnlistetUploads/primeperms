package de.primeapi.primeperms.core.managers.messages;

import com.google.inject.Inject;
import de.primeapi.primeperms.core.PrimePermsCore;
import de.primeapi.primeperms.core.api.yml.YamlConfiguration;
import lombok.Getter;

import java.io.File;
import java.io.IOException;

public class MessageManager {

    @Getter
    private static MessageManager instance;

    File file;

    @Inject
    YamlConfiguration cfg;

    public MessageManager(){
        instance = this;

        file = new File("plugins/PrimePerm", "messages.json");

        if(!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        cfg.setup(file);

        PrimePermsCore.getInstance().getThreadPoolExecutor().submit(() -> {
            int i = 0;
            for (Message message : Message.values()) {
                if (cfg.contains(message.getPath())) {
                    message.setContent(cfg.getString(message.getPath()).replaceAll("&", "§").replaceAll("%prefix%", Message.CORE_PREFIX.getContent()));
                } else {
                    String s = (message.getPrefix() ? "%prefix%" : "") + message.getContent().replaceAll("§", "&");
                    cfg.set(message.getPath(), s);
                    i++;
                    message.setContent(s.replaceAll("%prefix%", Message.CORE_PREFIX.getContent()).replaceAll("&", "§"));
                }
            }
            System.out.println("[PrimePerms] " + i + " Messages were loaded");
            cfg.save(file);
        });



    }

}
