package de.primeapi.primeperms.core.managers.messages;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
@Getter
public enum Message {

    CORE_PREFIX("§cPrime§c§lPerms §7|", false),

    PERMISSIONS_DESC("Verwallte deine Permissions", false),

    COMMANDS_USAGE_HEAD_1("§7§m----------[§r §6§lHilfe §8| §e%command% §7§m]----------", true),
    COMMANDS_USAGE_HEAD_2("", true),
    COMMANDS_USAGE_FORMAT("§7» §e%name% §8| §e%desc%", true),

    TIME_PERMANENT("Permanent", false),
    TIME_DAY("Tag", false),
    TIME_DAYS("Tage", false),
    TIME_HOUR("Stunde", false),
    TIME_HOURS("Stunden", false),
    TIME_MINUTE("Minute", false),
    TIME_MINUTES("Minuten", false),

    PLACEHOLDER("DO NOT REMOVE", false);


    String path;
    @Setter
    String content;
    Boolean prefix;

    Message(String content, Boolean prefix){
        this.content = content;
        this.prefix = prefix;
        this.path = this.toString().replaceAll("_", ".").toLowerCase();
    }


    public Message replace(String key, String value){
        if(!key.startsWith("%")){
            key = "%" + key + "%";
        }
        String s = getContent().replaceAll(key, value);
        PLACEHOLDER.setContent(s);
        return PLACEHOLDER;
    }

    public Message replace(String key, Object value) {
        return replace(key, String.valueOf(value));
    }
}
