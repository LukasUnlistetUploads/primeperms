package de.primeapi.primeperms.core;

import de.primeapi.primeperms.core.api.ServerType;
import de.primeapi.primeperms.core.io.Config;
import lombok.Getter;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

@Getter
public class PrimePermsCore {

    private static PrimePermsCore instance;
    private ThreadPoolExecutor threadPoolExecutor;
    private Config config;
    private final ServerType serverType;

    public static PrimePermsCore getInstance() {
        return instance;
    }

    public PrimePermsCore(ServerType serverType){
        instance = this;
        this.serverType = serverType;
        threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(5);


        config = new Config();

    }

}
