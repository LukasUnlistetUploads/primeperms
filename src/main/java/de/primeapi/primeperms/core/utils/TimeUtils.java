package de.primeapi.primeperms.core.utils;

import de.primeapi.primeperms.core.managers.messages.Message;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtils {

    public static Long toMillies(int days, int hours, int mins) {
        Long min = Long.valueOf(mins) * 60;
        Long hour = Long.valueOf(hours) * (60 * 60);
        Long day = Long.valueOf(days) * (60 * 60 * 24);
        Long time = min + hour + day;
        time = time * 1000;
        return time;
    }

    public String milliesToDate(Long millies){
        Date date = new Date(millies);
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH.mm");
        return df.format(millies);
    }

    public static String getEnd(Long end) {

        if(end == -1){
            return "Never";
        }

        String msg = "";

        long now = System.currentTimeMillis();
        long diff = end - now;
        long seconds = (diff / 1000);

        if (seconds >= 60 * 60 * 24) {
            long days = seconds / (60 * 60 * 24);
            seconds = seconds % (60 * 60 * 24);

            if (days <= 1) {
                msg += days + " " + Message.TIME_DAY.getContent() +  " ";
            } else {
                msg += days + " " + Message.TIME_DAYS.getContent() +  " ";
            }

        }
        if (seconds >= 60 * 60) {
            long h = seconds / (60 * 60);
            seconds = seconds % (60 * 60);

            if (h <= 1) {
                msg += h + " " + Message.TIME_HOUR.getContent() +  " ";
            } else {
                msg += h + " " + Message.TIME_HOURS.getContent() +  " ";
            }

        }
        if (seconds >= 60) {
            long min = seconds / 60;
            seconds = seconds % 60;

            if (min <= 1) {
                msg += min + " " + Message.TIME_MINUTE.getContent() +  " ";
            } else {
                msg += min + " " + Message.TIME_MINUTES.getContent() +  " ";
            }
        }

        return msg;
    }

}
