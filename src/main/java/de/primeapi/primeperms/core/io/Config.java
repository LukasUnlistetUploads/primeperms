package de.primeapi.primeperms.core.io;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.$Gson$Preconditions;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.annotation.processing.Filer;
import java.io.*;
import java.util.Scanner;

public class Config {

    public File file;

    public Config(){

        File ord = new File("plugins/PrimePerms");
        if(!ord.exists()) ord.mkdir();

        file = new File("plugins/PrimePerms", "config.json");
        if(!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        JSONObject object = new JSONObject();
        JSONObject mysql = new JSONObject();
        mysql.put("host", "localhost:3306");
        mysql.put("database", "permissions");
        mysql.put("username", "root");
        mysql.put("password", "password");;

        object.put("mysql", mysql);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String out = gson.toJson(object);

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write(out);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }



    }

    public String getMySQLString(String name){
        String s = "";
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()){
                s += scanner.nextLine();
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            JSONObject object = (JSONObject) new JSONParser().parse(s);
            return (String) ((JSONObject)object.get("mysql")).get(name);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

}
