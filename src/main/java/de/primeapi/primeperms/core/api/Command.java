package de.primeapi.primeperms.core.api;

import de.primeapi.primeperms.core.api.commandexecutor.CommandExecutor;
import de.primeapi.primeperms.core.managers.messages.Message;
import lombok.Getter;

@Getter
public abstract class Command {

    String name;
    String desc;
    Command[] subCommands;

    public Command(String name, String desc, Command... subCommands){
        this.name = name;
        this.desc = desc;
        this.subCommands = subCommands;
    }

    public void sendSubCommands(CommandExecutor commandExecutor, String parents){
        if(subCommands.length > 0) {
            commandExecutor.sendMessage(Message.COMMANDS_USAGE_HEAD_1.replace("command", getName()));
            commandExecutor.sendMessage(Message.COMMANDS_USAGE_HEAD_2);

            for (Command subCommand : subCommands) {
                commandExecutor.sendMessage(Message.COMMANDS_USAGE_FORMAT.replace("name", parents + " " + subCommand.getName()).replace("desc", subCommand.getDesc()));
            }
            commandExecutor.sendMessage(Message.COMMANDS_USAGE_HEAD_2);
            commandExecutor.sendMessage(Message.COMMANDS_USAGE_HEAD_1.replace("command", getName()));
        }
    }

    public boolean checkSubCommands(CommandExecutor commandExecutor, String[] args, String[] completeArgs){
        if(args.length < 1) return false;
        for(Command subCommand : subCommands){
            if(args[0].equalsIgnoreCase(subCommand.getName())){
                String[] newargs = {};
                System.arraycopy(args, 1, newargs, 0, args.length - 1);
                subCommand.execute(commandExecutor, newargs, completeArgs);
                return true;
            }
        }
        return false;
    }

    public abstract void execute(CommandExecutor commandExecutor, String[] args, String[] completeArgs);




}
