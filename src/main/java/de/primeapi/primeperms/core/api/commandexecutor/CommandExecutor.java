package de.primeapi.primeperms.core.api.commandexecutor;

import de.primeapi.primeperms.core.managers.messages.Message;

import java.util.UUID;

public interface CommandExecutor {


    UUID getUniqueId();
    String getName();

    void sendMessage(Message message);
    void sendNoPerms();
    boolean hasPermission(String permission);

}
