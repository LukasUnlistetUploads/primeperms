package de.primeapi.primeperms.core.api.yml;

import java.io.File;
import java.util.Objects;

public interface YamlConfiguration {

    void setup(File file);
    void set(String path, Object object);
    Object get(String path);
    String getString(String path);
    boolean contains(String path);
    void save(File file);
}
