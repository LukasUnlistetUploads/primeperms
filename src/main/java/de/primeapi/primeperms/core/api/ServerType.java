package de.primeapi.primeperms.core.api;

public enum ServerType {
    SPIGOT,
    BUNGEE
}
