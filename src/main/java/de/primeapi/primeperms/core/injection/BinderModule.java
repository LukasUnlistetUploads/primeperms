package de.primeapi.primeperms.core.injection;

import com.google.inject.AbstractModule;
import de.primeapi.primeperms.core.PrimePermsCore;
import de.primeapi.primeperms.core.api.ServerType;
import de.primeapi.primeperms.core.api.yml.BungeeConfiguration;
import de.primeapi.primeperms.core.api.yml.SpigotConfiguration;
import de.primeapi.primeperms.core.api.yml.YamlConfiguration;

public class BinderModule extends AbstractModule {
    @Override
    protected void configure() {
        if(PrimePermsCore.getInstance().getServerType() == ServerType.BUNGEE){
            bind(YamlConfiguration.class).toInstance(new BungeeConfiguration());
        }else {
            bind(YamlConfiguration.class).toInstance(new SpigotConfiguration());
        }
    }
}
