package de.primeapi.primeperms.core.commands;

import de.primeapi.primeperms.core.api.Command;
import de.primeapi.primeperms.core.api.commandexecutor.CommandExecutor;
import de.primeapi.primeperms.core.managers.messages.Message;

public class PermissionsCommand extends Command {


    public PermissionsCommand() {
        super("perms", Message.PERMISSIONS_DESC.getContent());
    }

    @Override
    public void execute(CommandExecutor p, String[] args, String[] com) {
        if(!p.hasPermission("perms.perms")) {
            p.sendNoPerms();
            return;
        }
        if(!checkSubCommands(p, args, com)){
            sendSubCommands(p, "/perms");
        }
    }
}
