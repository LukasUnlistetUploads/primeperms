package de.primeapi.primeperms.core.sql;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;

public class MySQL {

    public static Connection c;

    public static void connect(String host, String database, String username, String password) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            c = DriverManager.getConnection("jdbc:mysql://" + host + "/" + database + "?autoReconnect=true",username, password);
            System.out.println("[PrimePerms] MySQL-Verbindung aufgebaut.");
        } catch (SQLException e) {
            System.out.println("[PrimePerms] MySQL-Verbindung fehlgeschlagen: " + e.getMessage());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        setup();
    }

    public static void disconnect() {
        if (c != null) {
            try {
                c.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static void setup(){
        try {
            c.prepareStatement("CREATE TABLE IF NOT EXISTS `players` (" +
                    "`id` INT NOT NULL AUTO_INCREMENT," +
                    "`uuid` varchar(255) NOT NULL UNIQUE," +
                    "`name` varchar(255) NOT NULL UNIQUE," +
                    "`original_name` varchar(255) NOT NULL," +
                    "PRIMARY KEY (`id`)" +
                    ");").execute();
            c.prepareStatement("CREATE TABLE IF NOT EXISTS `ranks` (" +
                    "`id` INT NOT NULL AUTO_INCREMENT," +
                    "`name` varchar(255) NOT NULL UNIQUE," +
                    "`prefix` varchar(255)," +
                    "`suffix` varchar(255)," +
                    "`color` varchar(255) NOT NULL," +
                    "PRIMARY KEY (`id`)" +
                    ");").execute();
            c.prepareStatement("CREATE TABLE IF NOT EXISTS `player_permissions` (" +
                    "`id` INT NOT NULL AUTO_INCREMENT," +
                    "`player_id` INT NOT NULL UNIQUE," +
                    "`permission` VARCHAR(255) NOT NULL," +
                    "`assing` BOOLEAN NOT NULL DEFAULT true," +
                    "PRIMARY KEY (`id`)" +
                    ");").execute();
            c.prepareStatement("CREATE TABLE IF NOT EXISTS `rank_permissions` (" +
                    "`id` INT NOT NULL AUTO_INCREMENT UNIQUE," +
                    "`rank_id` INT NOT NULL," +
                    "`permission` varchar(255) NOT NULL," +
                    "`assing` BOOLEAN NOT NULL DEFAULT true," +
                    "PRIMARY KEY (`id`)" +
                    ");").execute();
            c.prepareStatement("CREATE TABLE IF NOT EXISTS `player_ranks` (" +
                    "`id` INT NOT NULL AUTO_INCREMENT UNIQUE," +
                    "`player_id` INT NOT NULL," +
                    "`rank_id` INT NOT NULL," +
                    "`strengh` INT NOT NULL," +
                    "`referrer` INT NOT NULL," +
                    "`assigned_time` BIGINT," +
                    "`validUntil` BIGINT," +
                    "`removed_time` BIGINT," +
                    "PRIMARY KEY (`id`)" +
                    ");").execute();

            c.prepareStatement("CREATE TABLE `rank_inheritance` (\n" +
                    "\t`id` INT NOT NULL AUTO_INCREMENT UNIQUE,\n" +
                    "\t`rank_id` INT NOT NULL,\n" +
                    "\t`inhert_from` INT NOT NULL,\n" +
                    "\tPRIMARY KEY (`id`)\n" +
                    ");").execute();

            c.prepareStatement("ALTER TABLE `player_permissions` ADD CONSTRAINT `player_permissions_fk0` FOREIGN KEY (`player_id`) REFERENCES `players`(`id`);").execute();
            c.prepareStatement("ALTER TABLE `rank_permissions` ADD CONSTRAINT `rank_permissions_fk0` FOREIGN KEY (`rank_id`) REFERENCES `ranks`(`id`);").execute();
            c.prepareStatement("ALTER TABLE `player_ranks` ADD CONSTRAINT `player_ranks_fk0` FOREIGN KEY (`player_id`) REFERENCES `players`(`id`);").execute();
            c.prepareStatement("ALTER TABLE `player_ranks` ADD CONSTRAINT `player_ranks_fk1` FOREIGN KEY (`rank_id`) REFERENCES `ranks`(`id`);").execute();
            c.prepareStatement("ALTER TABLE `player_ranks` ADD CONSTRAINT `player_ranks_fk2` FOREIGN KEY (`referrer`) REFERENCES `players`(`id`);").execute();
            c.prepareStatement("ALTER TABLE `rank_inheritance` ADD CONSTRAINT `rank_inheritance_fk0` FOREIGN KEY (`rank_id`) REFERENCES `ranks`(`id`);");
            c.prepareStatement("ALTER TABLE `rank_inheritance` ADD CONSTRAINT `rank_inheritance_fk1` FOREIGN KEY (`inhert_from`) REFERENCES `ranks`(`id`);");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void createPlayer(UUID uuid, String name){
        try {
            PreparedStatement st = c.prepareStatement("INSERT INTO players value (id,?,?,?)");
            st.setString(1, uuid.toString());
            st.setString(2, name.toLowerCase());
            st.setString(3, name);
            st.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void createRank(String name){
        try {
            PreparedStatement st = c.prepareStatement("INSERT INTO ranks value (id,?,?,?,?)");
            st.setString(1, name.toLowerCase());
            st.setString(2, null);
            st.setString(3, null);
            st.setString(4, null);
            st.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

}
