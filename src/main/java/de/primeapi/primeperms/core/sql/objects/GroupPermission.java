package de.primeapi.primeperms.core.sql.objects;

import de.primeapi.primeperms.core.sql.MySQL;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@RequiredArgsConstructor
public class GroupPermission {

    @NonNull
    final int id;

    public String getPermission(){
        String s = null;
        try{
            PreparedStatement st = MySQL.c.prepareStatement("SELECT `permission` FROM rank_permissions WHERE id=?");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if(!rs.next()) throw new IllegalArgumentException("rankPermission with id " + id + " doens't exists");
            s = rs.getString("permission");
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return s;
    }

    public Boolean assing(){
        Boolean b = null;
        try{
            PreparedStatement st = MySQL.c.prepareStatement("SELECT `assing` FROM rank_permissions WHERE id=?");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if(!rs.next()) throw new IllegalArgumentException("rankPermission with id " + id + " doens't exists");
            b = rs.getBoolean("assing");
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return b;
    }

}
