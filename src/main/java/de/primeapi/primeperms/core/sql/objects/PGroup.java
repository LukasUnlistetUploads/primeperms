package de.primeapi.primeperms.core.sql.objects;

import de.primeapi.primeperms.core.sql.MySQL;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class PGroup {

    @NonNull
    final int id;

    public PGroup(String name){
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT `id` FROM ranks WHERE name=?");
            st.setString(1, name.toLowerCase());
            ResultSet rs = st.executeQuery();
            this.id = rs.getInt("id");
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throw new IllegalArgumentException("Group with name " + name + " not found");
        }
    }


    public String getPrefix(){
        String s = null;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT `prefix` FROM ranks WHERE id=?");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            rs.next();
            s = rs.getString("prefix");
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return s;
    }
    public String getSuffix(){
        String s = null;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT `suffix` FROM ranks WHERE id=?");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            rs.next();
            s = rs.getString("suffix");
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return s;
    }
    public String getColor(){
        String s = null;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT `color` FROM ranks WHERE id=?");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            rs.next();
            s = rs.getString("color");
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return s;
    }

    public List<PlayerPermission> getPlayerPermissions(){
        List<PlayerPermission> list = new ArrayList<>();
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT `id` FROM rank_permissions WHERE rank_id=?");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()){
                list.add(new PlayerPermission(rs.getInt("id")));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return list;
    }

    public List<GroupAllocation> getGroupAllocations(){
        List<GroupAllocation> list = new ArrayList<>();
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT `id` FROM player_ranks WHERE rank_id=? ORDER BY assigned_time DESC ");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()){
                //TODO check if active
                list.add(new GroupAllocation(rs.getInt("id")));
            }
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return list;
    }

    public List<GroupInheritence> getGroupInheritences(){
        List<GroupInheritence> list = new ArrayList<>();
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT `id` FROM rank_inheritance WHERE rank_id=?");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()){
                list.add(new GroupInheritence(rs.getInt("inhert_from")));
            }
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return list;
    }

}
