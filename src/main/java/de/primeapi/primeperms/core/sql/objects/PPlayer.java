package de.primeapi.primeperms.core.sql.objects;

import de.primeapi.primeperms.core.sql.MySQL;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.swing.*;
import java.security.Permissions;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
public class PPlayer {

    @NonNull
    final int id;

    public PPlayer(UUID uuid){
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT `id` FROM players WHERE uuid=?");
            st.setString(1, uuid.toString());
            ResultSet rs = st.executeQuery();
            rs.next();
            id = rs.getInt("id");
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throw new IllegalArgumentException("Player with uuid " + uuid.toString() + " not found");
        }
    }

    public PPlayer(String name){
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT `id` FROM players WHERE name=?");
            st.setString(1, name);
            ResultSet rs = st.executeQuery();
            rs.next();
            id = rs.getInt("id");
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throw new IllegalArgumentException("Player with name " + name + " not found");
        }
    }

    public String getOriginalName(){
        String s = null;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT `original_name` FROM players WHERE id=?");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            rs.next();
            s = rs.getString("original_name");
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return s;
    }

    public List<PlayerPermission> getPlayerPermissions(){
        List<PlayerPermission> list = new ArrayList<>();
            try {
                PreparedStatement st = MySQL.c.prepareStatement("SELECT `id` FROM player_permissions WHERE player_id=?");
                st.setInt(1, id);
                ResultSet rs = st.executeQuery();
                while (rs.next()){
                    list.add(new PlayerPermission(rs.getInt("id")));
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        return list;
    }

    public List<GroupAllocation> getGroupAllocations(){
        List<GroupAllocation> list = new ArrayList<>();
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT `id` FROM player_ranks WHERE player_id=? ORDER BY strengh ");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                GroupAllocation allocation = new GroupAllocation(rs.getInt("id"));
                if (allocation.getRemovedTime() > 0) continue;
                if(allocation.getValidUntil() > System.currentTimeMillis()) continue;
                list.add(allocation);
            }
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return list;
    }

    public List<GroupAllocation> getAssingedGroupAllocations(){
        List<GroupAllocation> list = new ArrayList<>();
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT `id` FROM player_ranks WHERE referrer=? ORDER BY assigned_time DESC");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()){
                list.add(new GroupAllocation(rs.getInt("id")));
            }
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return list;
    }

    public void setName(String name){
        try {
            PreparedStatement st = MySQL.c.prepareStatement("UPDATE players SET name=? AND original_name=? WHERE id=?");
            st.setString(1, name.toLowerCase());
            st.setString(2, name);
            st.setInt(3, id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void addPermission(String permission, boolean assing){
        try {
            PreparedStatement st = MySQL.c.prepareStatement("INSERT INTO player_permissions value (id,?,?,?)");
            st.setInt(1, id);
            st.setString(2, permission.toLowerCase());
            st.setBoolean(3, assing);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    //public void addRank(PGroup rank, int strenght, PPlayer admin, )

    //TODO getPermissions



}
