package de.primeapi.primeperms.core.sql.objects;

import de.primeapi.primeperms.core.sql.MySQL;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@RequiredArgsConstructor
public class GroupAllocation {

    @NonNull
    final int id;

    public PPlayer getPermissionsPlayer(){
        int player_id = 0;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT `player_id` FROM player_ranks WHERE id=?");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            rs.next();
            player_id = rs.getInt("player_id");
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return new PPlayer(player_id);
    }
    public PPlayer getReferrer(){
        int player_id = 0;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT `referrer` FROM player_ranks WHERE id=?");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            rs.next();
            player_id = rs.getInt("referrer");
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return new PPlayer(player_id);
    }

    public PGroup getPermissionsGroup(){
        int player_id = 0;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT `rank_id` FROM player_ranks WHERE id=?");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            rs.next();
            player_id = rs.getInt("rank_id");
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return new PGroup(player_id);
    }

    public Integer getStrenght(){
        int i = 0;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT `strengh` FROM player_ranks WHERE id=?");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            i = rs.getInt("strengh");
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return i;
    }

    public Long getAssingedTime(){
        long l = 0;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT `strengh` FROM player_ranks WHERE id=?");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            l = rs.getInt("assigned_time");
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return l;
    }

    public Long getValidUntil(){
        long l = 0;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT `strengh` FROM player_ranks WHERE id=?");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            l = rs.getInt("validUntil");
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return l;
    }

    public Long getRemovedTime(){
        long l = 0;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT `strengh` FROM player_ranks WHERE id=?");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            l = rs.getInt("removed_time");
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return l;
    }

    


}
