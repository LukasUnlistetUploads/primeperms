package de.primeapi.primeperms.core.sql.objects;

import de.primeapi.primeperms.core.sql.MySQL;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@RequiredArgsConstructor @Getter
public class GroupInheritence {

    @NonNull
    final int id;

    public PGroup getInheriter(){
        int i = 0;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT `rank_id` FROM rank_inheritance WHERE id=?");
            st.setInt(1, id);

            ResultSet rs = st.executeQuery();
            rs.next();
            i = rs.getInt("rank_id");
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return new PGroup(i);
    }

    public PGroup getInheritence(){
        int i = 0;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT `rank_id` FROM rank_inheritance WHERE id=?");
            st.setInt(1, id);

            ResultSet rs = st.executeQuery();
            rs.next();
            i = rs.getInt("inhert_from");
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return new PGroup(i);
    }

}
